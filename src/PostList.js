import React from 'react';
import { List, Avatar } from 'antd';
import IconText from './IconText';

const PostList = (props) => (
  <List
    bordered
    itemLayout="vertical"
    dataSource={props.posts}
    renderItem={item => (
      <List.Item
        key={item.data.id}
        actions={[<IconText type="star-o" text="156" />, <IconText type="like-o" text="156" />, <IconText type="message" text="2" />]}
        extra={<img width={272} alt="logo" src={item.data.thumbnail} />}
      >
        <List.Item.Meta
          avatar={<Avatar src={item.data.thumbnail} />}
          title={<a href={item.href}>{item.data.title}</a>}
          description={item.data.permalink}
        />
        {item.data.title}
      </List.Item>
    )}
  />
)

export default PostList;
