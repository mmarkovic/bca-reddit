import React, { Component } from 'react';
import axios from 'axios';
import { Layout, Row, Input, Button, Spin } from 'antd';
import './App.css';
import PostList from './PostList';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      posts: [],
      searchString: '',
      loading: false,
    }
  }

  componentWillMount() {
    this.setState({loading: true})
    axios.get('https://www.reddit.com/r/funny.json')
      .then((resp) => {
        this.setState({ posts: resp.data.data.children, loading: false })
      })
      .catch((err) => {
        this.setState({loading: false})
      });
  }

  onSearchInputChange(e) {
    this.setState({ searchString: e.target.value })
  }

  searchReddit() {
    this.setState({loading: true})
    axios.get('https://www.reddit.com/search.json?q=' + this.state.searchString)
      .then((resp) => {
        this.setState({ posts: resp.data.data.children, loading: false })
      })
      .catch((err) => {
        this.setState({loading: false})
      })
  }

  render() {
    return (
      <Layout>
        <Spin spinning={this.state.loading}>
          <Row style={{ margin: 10, paddingBottom: 10 }}>
            <Input
              style={{width: 300}}
              placeholder="Search term"
              onChange={this.onSearchInputChange.bind(this)}
              onPressEnter={this.searchReddit.bind(this)}
              value={this.state.searchString}
            />
            <Button type="primary" icon="search" onClick={this.searchReddit.bind(this)} />
          </Row>
          <Row>
            <PostList posts={this.state.posts} />
          </Row>
        </Spin>
      </Layout>
    );
  }
}

export default App;
